/*
 * analog.h
 *
 *  Created on: Oct 19, 2020
 *      Author: fernanda
 */

#ifndef INC_ANALOG_H_
#define INC_ANALOG_H_
#include "FreeRTOS.h"
#include "cmsis_os.h"

#define CMD_AD "ad"


#define CHANNELS 2

BaseType_t ad_init();

BaseType_t ad_deinit();

void ad_task_fn(const void *par);

void ad_vis_fn(const void *par);

void cmd_ad(int argc, char **argv);

uint8_t ad_get(uint32_t *result, TickType_t wait);

#endif /* INC_ANALOG_H_ */
