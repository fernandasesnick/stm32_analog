/*
 * output.h
 *
 *  Created on: 2020. dec. 11.
 *      Author: fernanda
 */

#ifndef INC_OUTPUT_H_
#define INC_OUTPUT_H_

#include "FreeRTOS.h"

#define CMD_OUT "od"
#define OUTPUT_WRITE_MS (20)
#define TEMP_MAX 100
#define TEMP_MIN 0
#define AD_MAX 4095
#define AD_MIN 0

#define TB_CHANNEL 0
#define TF_CHANNEL 1

#define TEMPERATURE_LOST_BY_HEAT_CHANGER 5

#define SWITCH_OFF_TIME 5000
#define SWITCH_ON_TIME 5000

enum pump_state{
	OFF,
	ON,
	HOLD
};


BaseType_t od_init();

void pump_ctrl();

uint8_t pump_logic();

uint32_t elapsed_time_from_last_switch();

void output_main();

long map(long x, long in_min, long in_max, long out_min, long out_max);


void cmd_out(int argc, char **argv);

#endif /* INC_OUTPUT_H_ */
