/*
 * output.c
 *
 *  Created on: 2020. dec. 11.
 *      Author: fernanda
 */

#include "output.h"
#include "shell.h"
#include "analog.h"
#include <string.h>
#include "main.h"

typedef struct{
	uint32_t value_from_input[CHANNELS];
	uint32_t temperature[CHANNELS];
	uint32_t data_receive_succes;
	uint32_t switch_off_time;
	uint32_t switch_on_time;
	uint32_t last_switch_time;
	uint32_t heat_changer_lost;
	uint8_t pump_state;
	uint8_t analog_is_started;
}od_task_ctrl_t;


od_task_ctrl_t od_task_ctrl;

BaseType_t od_init(){

	bzero(&od_task_ctrl, sizeof(od_task_ctrl));
	od_task_ctrl.switch_off_time=SWITCH_OFF_TIME;
	od_task_ctrl.switch_on_time=SWITCH_ON_TIME;
	od_task_ctrl.heat_changer_lost=TEMPERATURE_LOST_BY_HEAT_CHANGER;

	return pdPASS;

}

void output_main(){
	od_task_ctrl.analog_is_started=ad_get(od_task_ctrl.value_from_input, ((TickType_t) 10) == pdTRUE);

	if(od_task_ctrl.analog_is_started){
		od_task_ctrl.data_receive_succes++;

		for(int i=0;i<CHANNELS;i++){
			od_task_ctrl.temperature[i]=map(od_task_ctrl.value_from_input[i], AD_MIN, AD_MAX, TEMP_MIN, TEMP_MAX);
		}

		pump_ctrl();
	}
}

void pump_ctrl(){
		switch(pump_logic()){
			case ON:
				sh_printf("pump turning ON..\r\n");
			    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
			    od_task_ctrl.last_switch_time=HAL_GetTick();
			    od_task_ctrl.pump_state=ON;
				break;

			case OFF:
				sh_printf("pump turning OFF..\r\n");
			    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
			    od_task_ctrl.last_switch_time=HAL_GetTick();
			    od_task_ctrl.pump_state=OFF;
				break;
			default:
				break;
			}
}

uint8_t pump_logic(){
	if(od_task_ctrl.temperature[TB_CHANNEL]>od_task_ctrl.temperature[TF_CHANNEL]+od_task_ctrl.heat_changer_lost && elapsed_time_from_last_switch()>od_task_ctrl.switch_on_time && !od_task_ctrl.pump_state){
		return ON;
	}
	if(od_task_ctrl.temperature[TB_CHANNEL]<od_task_ctrl.temperature[TF_CHANNEL]+od_task_ctrl.heat_changer_lost && elapsed_time_from_last_switch()>od_task_ctrl.switch_off_time && od_task_ctrl.pump_state){
		return OFF;
	}
return HOLD;
}


uint32_t elapsed_time_from_last_switch(){
	return HAL_GetTick() - od_task_ctrl.last_switch_time;
}


long map(long x, long in_min, long in_max, long out_min, long out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


#define CMD_HELP "help"
#define CMD_STAT "stat"
#define CMD_SWITCH "sw"
#define CMD_PUMP "pump"
#define CMD_LOG "log"

static void cmd_usage(){

	sh_printf(" usage: %s <help|stat|sw|pump|log> \r\n", CMD_OUT);

}
void cmd_out(int argc, char **argv){
	if(argc<=0  || strcasecmp(CMD_HELP, argv[0])==0){
		cmd_usage();
		return;
	}
	if(strcasecmp(CMD_STAT, argv[0])==0){
		sh_printf("last switch time: %d\r\n",od_task_ctrl.last_switch_time);
		sh_printf("All received data: %d\r\n",od_task_ctrl.data_receive_succes);
		for(int i=0;i<CHANNELS;i++){
					sh_printf("channel: %d Input: %d Temperature:%d\r\n",i,od_task_ctrl.value_from_input[i],od_task_ctrl.temperature[i]);
		}

		return;
	}
	if(strcasecmp(CMD_PUMP, argv[0])==0){
			sh_printf("last switch time: %d sec ago\r\n",(HAL_GetTick()-od_task_ctrl.last_switch_time)/1000);
			sh_printf((od_task_ctrl.pump_state)?"Pump is ON\n\r":"Pump is OFF\r\n");
			return;
		}
	if(strcasecmp(CMD_LOG, argv[0])==0){
			for(int i=0;i<CHANNELS;i++) sh_printf("temperature in %d channel: %d\r\n",i,od_task_ctrl.temperature[i]);
			sh_printf((od_task_ctrl.pump_state)?"Pump is ON\n\r":"Pump is OFF\r\n");
				return;
	}
	if(strcasecmp(CMD_SWITCH, argv[0])==0){
			if(!od_task_ctrl.pump_state){
				 sh_printf("pump turning ON..\r\n");
				 HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
				 od_task_ctrl.last_switch_time=HAL_GetTick();
				 od_task_ctrl.pump_state=ON;
			}else{
				 sh_printf("pump turning OFF..\r\n");
				 HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
				 od_task_ctrl.last_switch_time=HAL_GetTick();
				 od_task_ctrl.pump_state=OFF;
			}
			return;
		}

		cmd_usage();
	}
