/*
 * analog.c
 *
 *  Created on: Oct 19, 2020
 *      Author: fernanda
 */


#include "analog.h"


#undef DEBUG_MODULE //define
#include "shell.h"
#include <string.h>
#include "cmsis_os.h"
#include "semphr.h"

#define AD_MAX_VALUE (4096)
#define AD_MIN_VALUE (1500)

#define NOTIFY_WAIT_MS (100)
#define CONV_INTERVAL_MS (20)
#define AD_TASK_SLEEP_TIME (100)

#define EVT_AD_CONV_END (1<<0)
#define EVT_AD_CONV_START (1<<1)
#define EVT_AD_CONV_STOP (1<<2)

#define EVT_AD_MASK ((EVT_AD_CONV_END)| (EVT_AD_CONV_START) | (EVT_AD_CONV_STOP))
#define MOVING_AVERAGE_QUEUE_SIZE 10

/**
 * moving hyst, delta
 */
#define MOVING_HYSTERESIS_DELTA 10
typedef struct {
	uint32_t start_error;
	uint32_t stop_errors;
	uint32_t conv_success;
	uint32_t conv_error;
}ad_statistics_t;

typedef struct{
	uint32_t delta;
	uint32_t hyst_min;
	uint32_t hyst_max;
}moving_hysteresis_t;

typedef struct{
	uint32_t queue[MOVING_AVERAGE_QUEUE_SIZE];
	uint8_t index;
	uint32_t average;
}moving_average_t;

typedef struct {
	volatile uint8_t running;
	volatile uint8_t vis_running;
	uint16_t raw_value[CHANNELS];
	uint32_t current_value[CHANNELS];
	ad_statistics_t statistics;
	moving_hysteresis_t moving_hysteresis[CHANNELS];
	moving_average_t moving_avg[CHANNELS];
}ad_task_crtl_t;

static ad_task_crtl_t ad_task_ctrl;

extern ADC_HandleTypeDef hadc1;
extern osMutexId_t mtx_adHandle;
extern osThreadId_t VisTaskHandle;

extern osThreadId_t ADTASKHandle;

BaseType_t ad_init(){

	bzero(&ad_task_ctrl, sizeof(ad_task_ctrl));
	for(int i=0;i<CHANNELS;i++){
		ad_task_ctrl.moving_hysteresis[i].hyst_min = 0;
		ad_task_ctrl.moving_hysteresis[i].hyst_max = MOVING_HYSTERESIS_DELTA;
		ad_task_ctrl.moving_hysteresis[i].delta = MOVING_HYSTERESIS_DELTA;
	}

	return pdPASS;

}

BaseType_t ad_deinit(){
	return pdPASS;
}

//INTERR ENTRY POINT
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc){
	++ ad_task_ctrl.statistics.conv_success;
	BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xTaskNotifyFromISR(ADTASKHandle, EVT_AD_CONV_END, eSetBits, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
//conversion error it
void HAL_ADC_ErrorCallback(ADC_HandleTypeDef *hadc){

	HAL_ADC_Stop_DMA(&hadc1);
	++ ad_task_ctrl.statistics.conv_error;
}

static void start_ad_conversion(){
	BaseType_t result = HAL_ADC_Start_DMA(&hadc1, ad_task_ctrl.raw_value , 2);
	if(result != HAL_OK)
		++ad_task_ctrl.statistics.start_error;
	else
		ad_task_ctrl.running =1;
}

static uint32_t add_moving_hysteresis(uint32_t input, uint16_t channel_index){
	if(input> ad_task_ctrl.moving_hysteresis[channel_index].hyst_max){ //out of range in pos direction
		ad_task_ctrl.moving_hysteresis[channel_index].hyst_max += ad_task_ctrl.moving_hysteresis[channel_index].delta;
		ad_task_ctrl.moving_hysteresis[channel_index].hyst_min += ad_task_ctrl.moving_hysteresis[channel_index].delta;
		return ad_task_ctrl.moving_hysteresis[channel_index].hyst_max;

	}
	if(input< ad_task_ctrl.moving_hysteresis[channel_index].hyst_min){ //out of range in neg direction
		ad_task_ctrl.moving_hysteresis[channel_index].hyst_max -= ad_task_ctrl.moving_hysteresis[channel_index].delta;
		ad_task_ctrl.moving_hysteresis[channel_index].hyst_min -= ad_task_ctrl.moving_hysteresis[channel_index].delta;
		return ad_task_ctrl.moving_hysteresis[channel_index].hyst_min;
	}
	return input;
}

static void add_moving_average(uint32_t value, uint16_t channel_index){

	ad_task_ctrl.moving_avg[channel_index].queue[ad_task_ctrl.moving_avg[channel_index].index] = value;
	ad_task_ctrl.moving_avg[channel_index].index = (ad_task_ctrl.moving_avg[channel_index].index +1) % MOVING_AVERAGE_QUEUE_SIZE;
	uint32_t sum =0;
	for(uint8_t i=0; i< MOVING_AVERAGE_QUEUE_SIZE; i++ ){
		sum += ad_task_ctrl.moving_avg[channel_index].queue[i];

		ad_task_ctrl.moving_avg[channel_index].average = sum/MOVING_AVERAGE_QUEUE_SIZE;
	}
}

void ad_task_fn(const void *par){

	uint32_t notified_value;
	BaseType_t result;
	for(;;){

		result = xTaskNotifyWait(EVT_AD_MASK,EVT_AD_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

		pr_debug("Leave AD Task notify, its are 0x%x",notified_value);

		if(notified_value & EVT_AD_CONV_STOP){
			//ad_task_crtl_t.running=0;
			ad_task_ctrl.running=0;
			result=HAL_ADC_Stop_DMA(&hadc1);
			if(result!=HAL_OK){
				++ad_task_ctrl.statistics.conv_error;
				pr_err("AD stop error");
			}
		}

		if(notified_value & EVT_AD_CONV_START){
			pr_debug("conversion start");
			start_ad_conversion();
		}

		if(notified_value &EVT_AD_CONV_END){
			pr_debug("Conv end, raw value: %d", ad_task_ctrl.raw_value[0]);


			for(int i=0;i<CHANNELS;i++){
				add_moving_average(add_moving_hysteresis(ad_task_ctrl.raw_value[i],i),i);

			}

			if(xSemaphoreTake(mtx_adHandle, ((TickType_t) 10) == pdTRUE)==pdTRUE){
				for(int i=0;i<CHANNELS;i++){
					ad_task_ctrl.current_value[i]=ad_task_ctrl.moving_avg[i].average;
				}
				xSemaphoreGive(mtx_adHandle);
			}



			++ad_task_ctrl.statistics.conv_success;
			osDelay(CONV_INTERVAL_MS);
			if(!ad_task_ctrl.running)
				continue;
			start_ad_conversion();
		}
	}
}

uint8_t ad_get(uint32_t *result, TickType_t wait){
	assert_param(result);

	if(xSemaphoreTake(mtx_adHandle, wait)==pdTRUE){
		memcpy(result, ad_task_ctrl.current_value, sizeof(ad_task_ctrl.current_value));
		xSemaphoreGive(mtx_adHandle);
		return pdPASS;
	}
	return pdFALSE;

}

#define VISUALIZATION_MAX_VALUE 40//many char inside gtkterm

#define EVT_AD_VIS_START (1<<3) //1 SHIFT LEFT 2 BITS
#define EVT_AD_VIS_STOP (1<<4)
#define EVT_VIS_MASK ((EVT_AD_VIS_START) | (EVT_AD_CONV_STOP))
#define TASK_VIS_SLEEP_TIME_MS (100)

static inline void vis_value(char *buffer, uint32_t v){
	bzero(buffer, VISUALIZATION_MAX_VALUE +1);
	memset(buffer, '=',v);
	pr_info(buffer);
}

static inline uint32_t min(uint32_t a, uint32_t b){
	return a<b?a:b;
}

void ad_vis_fn(const void *par){
	char visual_buffer[VISUALIZATION_MAX_VALUE +1];

	for(;;){
		uint32_t notified_value =0;
		xTaskNotifyWait(0, EVT_VIS_MASK, &notified_value, pdMS_TO_TICKS(NOTIFY_WAIT_MS));

		if(notified_value & EVT_AD_VIS_START){
			ad_task_ctrl.vis_running =1;
		}
		if(notified_value & EVT_AD_VIS_STOP){
			ad_task_ctrl.vis_running =0;
			vis_value(visual_buffer,0);//clear the screen
			goto tsk_wait;
		}

		if(ad_task_ctrl.vis_running){
			uint32_t current =0;
			for(int i=0;i<CHANNELS;i++){
				if(!ad_get(&current, osWaitForever)){
					goto tsk_wait;
				}
			}

		current= (current-AD_MIN_VALUE)* VISUALIZATION_MAX_VALUE /(AD_MAX_VALUE -AD_MIN_VALUE);
		uint32_t size = min(VISUALIZATION_MAX_VALUE, current);
		vis_value(visual_buffer,size);
		}

tsk_wait:
	osDelay(TASK_VIS_SLEEP_TIME_MS);
	}
}



#define CMD_HELP "help"
#define CMD_START "start"
#define CMD_STOP "stop"
#define CMD_STAT "stat"
#define CMD_VIS "vis"

static void cmd_usage(){

	sh_printf(" usage: %s <help|stat|start|vis<start|stop>> \r\n", CMD_AD);

}
void cmd_ad(int argc, char **argv){
	if(argc<=0  || strcasecmp(CMD_HELP, argv[0])==0){
		cmd_usage();
		return;
	}
	if(strcasecmp(CMD_STAT, argv[0])==0){
		sh_printf("State is %s \r\n", ad_task_ctrl.running?"started":"stopped");
		sh_printf("conversion: %d, conv_errors: %d, start_errors: %d, stop_errors: %d \n\r",
				ad_task_ctrl.statistics.conv_success,
				ad_task_ctrl.statistics.conv_error,
				ad_task_ctrl.statistics.start_error,
				ad_task_ctrl.statistics.stop_errors );

		for(int i=0;i<CHANNELS;i++){
			sh_printf("current value is %d, raw value is: %d \r\n",ad_task_ctrl.moving_avg[i].average, ad_task_ctrl.raw_value[i]);
		}

		return;
	}
		if(strcasecmp (CMD_START, argv[0])==0){
			if(ad_task_ctrl.running){
				sh_printf("AD Task already started \n\r");
				return;
			}
			xTaskNotify(ADTASKHandle,EVT_AD_CONV_START, eSetBits);
			sh_printf("AD Task is starting\n\r");
			return;
		}
		if(strcasecmp (CMD_STOP, argv[0])==0){
			if(!ad_task_ctrl.running){
				sh_printf("AD Task already stopped\n\r");
				return;
			}
			xTaskNotify(ADTASKHandle,EVT_AD_CONV_STOP, eSetBits);
					sh_printf("AD Task is stopping\n\r");
					return;
		}

		if(argc ==2 && strcasecmp(CMD_VIS, argv[0])==0){
			if(strcasecmp(CMD_START, argv[1])==0){
				if(ad_task_ctrl.vis_running){
					sh_printf("Visualization already started\r\n");
					return;
				}
				xTaskNotify(VisTaskHandle, EVT_AD_VIS_START, eSetBits);
				sh_printf("Visualization is starting\r\n");
			}
			if(strcasecmp(CMD_STOP, argv[1])==0){
					if(ad_task_ctrl.vis_running){
						sh_printf("Visualization already stopped\r\n");
						return;
					}
					xTaskNotify(VisTaskHandle, EVT_AD_VIS_STOP, eSetBits);
					sh_printf("Visualization is stopping\r\n");
					return;
				}
		}
		cmd_usage();
	}













































